#ifndef _throttle_H
#define _throttle_H

//340 < forward 529, 299 < neutral < 309. 250 < Reverse < 290, 50Hz PWM
//ONLY ACCELERATION ALLOWED. DO NOT MAKE DRASTIC CHANGES TO PWM
#define FORWARD_MAX 529
#define FORWARD_MIN 340
#define FORWARD_SAFETY_MAX 340 //333
#define FWD_DEADBAND 22 //15
#define REVERSE_DEADBAND 5
#define THROTTLE_NEUTRAL 308
#define REVERSE_MAX 250
#define REVERSE_MIN 290
#define REVERSE_SAFETY_MIN 293

int throttle_limit_safety(int raw);
int to_throttle(int ten_level);

#endif
