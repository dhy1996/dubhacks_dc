#ifndef _steering_H
#define _steering_H

#define SERVO_MIN 310
#define SERVO_MAX 420
#define ANGLE_MIN -45
#define ANGLE_MAX 45
int get_steering_servo(int angle);

#endif
