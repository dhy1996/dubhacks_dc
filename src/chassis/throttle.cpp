#include <throttle.h>
#include <common_functs.h>

int throttle_limit_safety(int raw) {
	raw = raw > FORWARD_SAFETY_MAX ? FORWARD_SAFETY_MAX : raw;
	raw = raw < REVERSE_SAFETY_MIN ? REVERSE_SAFETY_MIN : raw;
	return raw;
}

int to_throttle(int ten_level) {
	if(ten_level > 0) {
		return map(ten_level, 0, 10, THROTTLE_NEUTRAL + FWD_DEADBAND, FORWARD_SAFETY_MAX);	
	} else if (ten_level < 0) {
		return map(ten_level, -10, 0, THROTTLE_NEUTRAL - REVERSE_DEADBAND, REVERSE_SAFETY_MIN);	
	}
	return THROTTLE_NEUTRAL;
}
