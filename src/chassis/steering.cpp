#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <time.h>
#include <JHPWMPCA9685.h>
#include <common_functs.h>
#include <steering.h>

int get_steering_servo(int angle) {
	//neg because shit is reversed
	return map(-angle, ANGLE_MIN, ANGLE_MAX, SERVO_MIN, SERVO_MAX);
}
