#include "ros/ros.h"
#include "std_msgs/String.h"

#include <string>
#include <JHPWMPCA9685.h>
#include <chassis.h>
#include <steering.h>
#include <throttle.h>
#include <math.h>

#define PWM_FREQUENCY 50
#define STEERING_CHANNEL 0
#define THROTTLE_CHANNEL 1

#define NORMAL 0
#define SHUTDOWN 1
#define REVERSE 2
#define STOP 3
#define RESTART 4

#define REVERSE_ARC_LIMIT 13750
#define REVERSE_INIT_STATE 0
#define REVERSE_2ND_HALF_STATE 1

int curr_throttle;
int curr_steering;
int state;
int reverse_tracking;
int throttle_setpt;
float throttle_kP = 0.3;
uint8_t reverse_state; //0 - completing first half, 1 - completing 2nd half

int main(int argc, char **argv) {
	//state variables init
	state = NORMAL;
	reverse_tracking = 0;
	reverse_state = 0;
    PCA9685 *pca9685 = new PCA9685() ;
    int err = pca9685->openPCA9685();
	curr_steering = get_steering_servo(0);
	curr_throttle = THROTTLE_NEUTRAL;
	throttle_setpt = THROTTLE_NEUTRAL;

	//PCA9685 I2C init
    if (err < 0){
        printf("Error: %d", pca9685->error);
    } else {
        printf("PCA9685 Device Address: 0x%02X\n",pca9685->kI2CAddress);
        pca9685->setAllPWM(0,0);
        pca9685->reset();
        pca9685->setPWMFrequency(PWM_FREQUENCY);
	}

	//ROS init
    ros::init(argc, argv, "chassis_listener");
    ros::NodeHandle n;
    ros::Subscriber sub = n.subscribe("control", 1000, control_callback);

    //Main loop
	//ros::Rate r(1);
	while (ros::ok()) {
	  throttle_pid();
	  set_throttle_and_steering(pca9685, curr_throttle, curr_steering);
	  ros::spinOnce();  
	  if(state == SHUTDOWN) {
	  	break;
	  } 
	  switch(state) {
	  	case REVERSE:
	  		reverse();
	  		break;

	  	case STOP:
	  		throttle_setpt = THROTTLE_NEUTRAL;
	  		curr_steering = get_steering_servo(0);
	  		break;

	  	case RESTART:
	  		state = NORMAL;
	  		break;

	  }       
	  //r.sleep();
	}

	//cleanup
	pca9685->setAllPWM(0,0);
	delete pca9685;
    return 0;
}

void control_callback(const std_msgs::String::ConstPtr& msg) {
	const char* c_txt = msg->data.c_str();
	ROS_INFO("I heard: [%s]", c_txt);
	std::string txt = std::string(c_txt);
	size_t comma_pos = txt.find(",");
	switch(comma_pos) {
		case -1:
			state = txt.length();
			break;

		default:
			if(state != NORMAL) {
				break;
			}
			std::string vel_str = txt.substr(0, comma_pos);
			std::string angle_str = txt.substr(comma_pos + 1);
			int vel; 
			int angle; 
			std::istringstream(vel_str) >> vel;
			std::istringstream(angle_str) >> angle;
			throttle_setpt = to_throttle(vel);
			curr_steering = get_steering_servo(angle);
	}
}

void set_throttle_and_steering(PCA9685 *pca9685, int throttle, int steering) {
	pca9685->setPWM(STEERING_CHANNEL,0,steering);
	pca9685->setPWM(THROTTLE_CHANNEL,0,throttle);
}

void reverse() {
	if (reverse_tracking >= REVERSE_ARC_LIMIT) {
		switch(reverse_state) {
			case REVERSE_INIT_STATE:
				reverse_tracking = 0;
				reverse_state = REVERSE_2ND_HALF_STATE;
				break;
			default:
				reverse_tracking = 0;
				reverse_state = REVERSE_INIT_STATE;
				state = NORMAL;
				return;
		}
	}
	switch(reverse_state) {
		case REVERSE_INIT_STATE:
			curr_steering = get_steering_servo(ANGLE_MAX);
			throttle_setpt = REVERSE_SAFETY_MIN;
			break;
		default:
			curr_steering = get_steering_servo(ANGLE_MIN);
			throttle_setpt = FORWARD_SAFETY_MAX;
			break;
	}
}

void throttle_pid() {
	int error = throttle_setpt - curr_throttle;
	curr_throttle += (int) round(throttle_kP * error);
	curr_throttle = throttle_limit_safety(curr_throttle);
}
