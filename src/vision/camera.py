#!/usr/bin/env python

import cv2
import rospy
import time
from sensor_msgs.msg import Image
from cv_bridge import CvBridge

class Camera:
    def __init__(self):
        self.pub = rospy.Publisher("cam",
                                   Image, #determine ros data structure for image data
                                   queue_size = 0)
        self.bridge = CvBridge()
        rospy.init_node('camera', anonymous=True)
        
        camera_index = 1
        cap = self.init_camera(camera_index)
        while not rospy.is_shutdown():
            ret,frame = cap.read()
            if ret:
                msg = self.bridge.cv2_to_imgmsg(frame, "bgr8")
                self.pub.publish(msg)
            else:
                camera_index -= 1
                cap = self.init_camera(camera_index)
        cap.release()

    def init_camera(self, index):
        cap = cv2.VideoCapture(index)
        cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1080)
        cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 720)
        if not cap.isOpened():
            cap.open(index)
            #cap.set(cv2.cv.CV_CAP_PROP_FPS, 1)
	return cap

if __name__ == '__main__':
    Camera()
