#!/usr/bin/env python

import cv2 as cv
import rospy
import os
from std_msgs.msg import String
import time
import numpy as np
import yaml
from sensor_msgs.msg import Image
from cv_bridge import CvBridge
import sys

CONTROL_TOPIC = "control"
STREAM_TOPIC = "stream"
STOPDISTANCE = 190
BRAKE_STRENGTH = 5

# A yaml constructor is for loading from a yaml node.
# This is taken from: http://stackoverflow.com/a/15942429
def opencv_matrix_constructor(loader, node):
    mapping = loader.construct_mapping(node, deep=True)
    mat = np.array(mapping["data"])
    mat.resize(mapping["rows"], mapping["cols"])
    return mat
yaml.add_constructor(u"tag:yaml.org,2002:opencv-matrix", opencv_matrix_constructor)

COLOR = 'green'
HISTOGRAM_PATH = os.path.dirname(__file__) + "/" + COLOR + '-histogram.yml'

class Controller:
    def __init__(self):
        with open(HISTOGRAM_PATH, 'r') as f:
            f.readline()
            self.hist = np.float32(yaml.load(f)["histogram"])
            
        #control
        self.target_pub = rospy.Publisher(CONTROL_TOPIC,
                           String, #determine ros data structure for image data
                           queue_size = 10)

        self.cam_sub = rospy.Subscriber("cam", Image, self.callback)
        
        #livestream
        self.stream_pub = rospy.Publisher(STREAM_TOPIC ,
                                   Image, #determine ros data structure for image data
                                   queue_size = 5)
        self.bridge = CvBridge()

        rospy.init_node("test_pub", anonymous=True)

        #greenLower = (29, 86, 50)
        #greenUpper = (65, 255, 255)
        self.kernel = np.ones((5,5),np.uint8)
	self.steering = 0
	self.throttle = 0
	self.prevWidth = 0
        self.currentx = 320
        self.currenty = 240
        
        rospy.spin()

    def callback(self, image_msg):
        frame = self.bridge.imgmsg_to_cv2(image_msg, "bgr8")
        blurred = cv.GaussianBlur(frame, (11, 11), 0)

        lab = cv.cvtColor(blurred, cv.COLOR_BGR2LAB)
        backproj = cv.calcBackProject([lab], [1, 2], self.hist, [0, 255, 0, 255], 1)
        ret, mask = cv.threshold(backproj, 75, 255, cv.THRESH_BINARY)

        #hsv = cv.cvtColor(blurred, cv.COLOR_BGR2HSV)
        #mask = cv.inRange(hsv, greenLower, greenUpper)
        mask = cv.erode(mask, self.kernel, iterations=2)
        mask = cv.dilate(mask, self.kernel, iterations=2)


        cnts = cv.findContours(mask.copy(), cv.RETR_EXTERNAL, cv.CHAIN_APPROX_SIMPLE)
        cnts = cnts[1]
        center = None
        width = 0


        if len(cnts) > 0:

            c = max(cnts, key=cv.contourArea)
            ((x, y), radius) = cv.minEnclosingCircle(c)
            M = cv.moments(c)
            self.currentx = int(M["m10"] / M["m00"])
            self.currenty = int(M["m01"] / M["m00"])
            center = (self.currentx, self.currenty)
            width = 2 * radius

            if radius > 5:

                cv.circle(frame, (int(x), int(y)), int(radius), (0, 255, 255), 2)
                cv.circle(frame, center, 5, (0, 0, 255), -1)
 
        if(width == 0):
            width = STOPDISTANCE

        derivative = self.prevWidth - width
        #print derivative
        self.steering = (self.currentx - 320) * (45.0/320)
        self.throttle = 10 - (width * (10.0/STOPDISTANCE))
        if(derivative > 5):
            self.throttle += 2
        elif(derivative < -5):
            self.throttle -= BRAKE_STRENGTH
        #cameraTilt = (currenty - 240) * (45.0/320)
        #print width
        #print str(cameraPan) + ' ' + str(cameraTilt)
        #cv.imshow('mask',mask)
        #cv.imshow('my camera',frame)
        self.target_pub.publish(str(int(self.throttle)) + "," + str(int(self.steering)))
        self.stream_pub.publish(self.bridge.cv2_to_imgmsg(frame, "bgr8"))
        self.prevWidth = width

if __name__ == "__main__":
	Controller()
            
     
