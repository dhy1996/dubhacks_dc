#!/usr/bin/env python

import numpy as np
import os
import six.moves.urllib as urllib
import sys
import tarfile
import tensorflow as tf
import zipfile

from collections import defaultdict
from io import StringIO
from matplotlib import pyplot as plt
from PIL import Image

import cv2

from sensor_msgs.msg import Image
from cv_bridge import CvBridge
# This is needed since the notebook is stored in the object_detection folder.
sys.path.append("..")


# ## Object detection imports
# Here are the imports from the object detection module.

# In[3]:

from utils import label_map_util

from utils import visualization_utils as vis_util


class Detector:
  def __init__(self):
    # # Model preparation 

    # ## Variables
    # 
    # Any model exported using the `export_inference_graph.py` tool can be loaded here simply by changing `PATH_TO_CKPT` to point to a new .pb file.  
    # 
    # By default we use an "SSD with Mobilenet" model here. See the [detection model zoo](https://github.com/tensorflow/models/blob/master/object_detection/g3doc/detection_model_zoo.md) for a list of other models that can be run out-of-the-box with varying speeds and accuracies.

    # In[4]:

    # What model to download.
    #MODEL_NAME = 'ssd_mobilenet_v1_coco_11_06_2017'
    #MODEL_NAME = 'ssd_mobilenet_v1_0.75_depth_300x300_coco14_sync_2018_07_03'
    #MODEL_NAME = 'ssd_mobilenet_v1_ppn_shared_box_predictor_300x300_coco14_sync_2018_07_03'
    #MODEL_NAME = 'ssd_mobilenet_v2_coco_2018_03_29'
    MODEL_NAME = 'ssdlite_mobilenet_v2_coco_2018_05_09'
    MODEL_FILE = MODEL_NAME + '.tar.gz'
    DOWNLOAD_BASE = 'http://download.tensorflow.org/models/object_detection/'

    # Path to frozen detection graph. This is the actual model that is used for the object detection.
    #PATH_TO_CKPT = MODEL_NAME + '/frozen_inference_graph.pb'
    PATH_TO_CKPT = MODEL_NAME + '/dubhacks_inference_graph.pb'

    # List of the strings that is used to add correct label for each box.
    #PATH_TO_LABELS = os.path.join('data', 'mscoco_label_map.pbtxt')
    PATH_TO_LABELS = os.path.join('data', 'dubhacks.pbtxt')

    NUM_CLASSES = 2

    '''
    # ## Download Model
    print("Downloading model")
    # In[5]:

    opener = urllib.request.URLopener()

    print("retrieving")
    opener.retrieve(DOWNLOAD_BASE + MODEL_FILE, MODEL_FILE)
    print("opening")
    tar_file = tarfile.open(MODEL_FILE)
    for file in tar_file.getmembers():
      print("processing file")
      file_name = os.path.basename(file.name)
      if 'frozen_inference_graph.pb' in file_name:
        print("extracting")
        tar_file.extract(file, os.getcwd())

    # ## Load a (frozen) Tensorflow model into memory.
    print("Loading model")
    '''

    # In[6]:

    self.detection_graph = tf.Graph()
    with detection_graph.as_default():
      od_graph_def = tf.GraphDef()
      with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
        serialized_graph = fid.read()
        od_graph_def.ParseFromString(serialized_graph)
        tf.import_graph_def(od_graph_def, name='')


    # ## Loading label map
    # Label maps map indices to category names, so that when our convolution network predicts `5`, we know that this corresponds to `airplane`.  Here we use internal utility functions, but anything that returns a dictionary mapping integers to appropriate string labels would be fine

    # In[7]:

    label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
    categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES, use_display_name=True)
    category_index = label_map_util.create_category_index(categories)


    # ## Helper code

    # In[8]:

    # # Detection
    print("Detection")

    # In[9]:

    # For the sake of simplicity we will use only 2 images:
    # image1.jpg
    # image2.jpg
    # If you want to test the code with your images, just add path to the images to the TEST_IMAGE_PATHS.
    PATH_TO_TEST_IMAGES_DIR = 'test_images'
    TEST_IMAGE_PATHS = [ os.path.join(PATH_TO_TEST_IMAGES_DIR, 'image{}.jpg'.format(i)) for i in range(1, 3) ]

    # Size, in inches, of the output images.
    IMAGE_SIZE = (12, 8)
    
    self.sess = tf.Session(graph=self.detection_graph)
    self.pub = rospy.Publisher("control",
                           String, #determine ros data structure for image data
                           queue_size = 10)
    self.sub = rospy.Subscriber("cam", Image, self.callback)
    self.frames_without_stop = 0
    self.is_stopped = False
    rospy.init_node('detector', anonymous=True)
    rospy.spin()
    
    # In[10]:
    #stop_count = 0
    def callback(self, image_msg):
      image_np = self.bridge.imgmsg_to_cv2(image_msg, "bgr8")
      #image_np = cv2.pyrUp(image_np)
      # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
      image_np_expanded = np.expand_dims(image_np, axis=0)
      image_tensor = self.detection_graph.get_tensor_by_name('image_tensor:0')
      # Each box represents a part of the image where a particular object was detected.
      boxes = self.detection_graph.get_tensor_by_name('detection_boxes:0')
      # Each score represent how level of confidence for each of the objects.
      # Score is shown on the result image, together with the class label.
      scores = self.detection_graph.get_tensor_by_name('detection_scores:0')
      classes = self.detection_graph.get_tensor_by_name('detection_classes:0')
      num_detections = self.detection_graph.get_tensor_by_name('num_detections:0')
      # Actual detection.
      (boxes, scores, classes, num_detections) = self.sess.run(
          [boxes, scores, classes, num_detections],
          feed_dict={image_tensor: image_np_expanded})
      # Visualization of the results of a detection.
      vis_util.visualize_boxes_and_labels_on_image_array(
          image_np,
          np.squeeze(boxes),
          np.squeeze(classes).astype(np.int32),
          np.squeeze(scores),
          category_index,
          use_normalized_coordinates=True,
          line_thickness=8)
      
      #print(classes[0][0])
      if (scores[0][0] >= 0.5 and classes[0][0] == 2) or (scores[0][1] >= 0.5 and classes[0][1] == 2):
        #stop_count += 1
        self.frames_without_stop = 0
        print("STOP " + str(stop_count))
        #our ros control uses string length to determine command value
        self.pub.publish("STO")
        self.is_stopped = True
      else:
        self.frames_without_stop += 1
        if self.frames_without_stop > 20 and self.is_stopped:
          self.pub.publish("STAR")
          self.is_stopped = False


def load_image_into_numpy_array(image):
  (im_width, im_height) = image.size
  return np.array(image.getdata()).reshape(
      (im_height, im_width, 3)).astype(np.uint8)

if __name__ == "__main__":
  Detector()
